<html
<head>
<style>
	.parent{
  	width: 200px;
	height: 200px;
  	position: relative;
  	-webkit-perspective: 500px;
}

.front, .back{
	text-align: center;
  	line-height: 200px;
  	font-size: 5em;
  	color: white;
  	font-family: sans-serif;
  	width: 100%;
  	height: 100%;
  	position: absolute;
  	left: 0; top:0;
  -webkit-transition: all 1s;
  
  -webkit-transform-style: preserve-3d; 
  -webkit-backface-visibility: hidden; 
}

.front {
  background-image: url('https://d301sr5gafysq2.cloudfront.net/8d5f806505b4/img/blank-state/overview.svg');
  //background-color: red;
}

.back {
  background-image: url('https://d301sr5gafysq2.cloudfront.net/8d5f806505b4/img/blank-state/pr-list-empty.svg');
 // background-color: blue;
  -webkit-transform: rotateY(180deg);
}

.parent:hover .front {
  -webkit-transform: rotateY(-180deg);
}

.parent:hover .back {
  -webkit-transform: rotateY(0deg);
}
</style>
</head>
	<body>
		<div class="parent">
		<div class="front">1</div>
		<div class="back">2</div>
		</div>
	</body>
</html>

